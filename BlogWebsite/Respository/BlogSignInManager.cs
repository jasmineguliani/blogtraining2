﻿using Blog.DB.Entities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace BlogWebsite.Respository
{
    // Configure the application sign-in manager which is used in this application.
    public class BlogSignInManager : SignInManager<BlogUser, string>
    {
        public BlogSignInManager(BlogUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(BlogUser user)
        {
            return user.GenerateUserIdentityAsync((BlogUserManager)UserManager);
        }

        public static BlogSignInManager Create(IdentityFactoryOptions<BlogSignInManager> options, IOwinContext context)
        {
            return new BlogSignInManager(context.GetUserManager<BlogUserManager>(), context.Authentication);
        }
    }
}