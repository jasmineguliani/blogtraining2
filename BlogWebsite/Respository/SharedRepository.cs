﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace BlogWebsite.Respository
{
    public class SharedRepository
    {

        public class EmailService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {             
                    SmtpClient client = new SmtpClient();
                    return client.SendMailAsync("gulianijasmin19@gmail.com",
                                                 message.Destination,
                                                 message.Subject,
                                                 message.Body);
                 
            }
        }

        public class SmsService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {
                // Plug in your SMS service here to send a text message.
                return Task.FromResult(0);
            }
        }
    }
}