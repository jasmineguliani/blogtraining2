﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace BlogWebsite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            RouteTable.Routes.MapHttpRoute("Blogs",
                "api/sitecore/Blogs", 
                new { controller = "Account", action = "Login" } 
            );

        }
    }
}
