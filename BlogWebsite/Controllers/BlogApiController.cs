﻿using Blog.DB.Entities;
using Blog.DB.Interfaces;
using BlogWebsite.Respository;
using log4net;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace BlogWebsite.Controllers
{
    public class BlogApiController : ApiController
    {
        IDatabaseRepository _dbRepo;
        public BlogApiController(IDatabaseRepository _dbRepository)
        {
            _dbRepo = _dbRepository;
        }

        [Route("api/BlogApi/GetArticles")]
        public IHttpActionResult GetArticles()
        {
            List<Article> articleList = new List<Article>();
            try
            {
                articleList = _dbRepo.GetAllArticles();
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(articleList);
                var response = this.Request.CreateResponse(HttpStatusCode.OK);
                if (!string.IsNullOrEmpty(jsonString))
                {
                    response.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                }
                return ResponseMessage(response);

            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex));
            }
        }

        [Route("api/BlogApi/GetArticleDetail/{id}")]
        public IHttpActionResult GetArticleDetail([FromUri]int id)
        {

            Article article = new Article();
            try
            {
                article = _dbRepo.GetArticleById(id);
                var response = this.Request.CreateResponse(HttpStatusCode.OK);
                if (article != null)
                {
                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(article);
                    response.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");

                }
                else
                {
                    response.Content = new StringContent("No article exists", Encoding.UTF8, "application/json");
                }
                return ResponseMessage(response);

            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex));
            }
        }

        [HttpDelete, Route("api/BlogApi/DeleteArticle/{id}")]
        public IHttpActionResult Delete([FromUri] string id)
        {

            Article article = new Article();
            try
            {
                _dbRepo.DeleteArticle(article);
                var response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent("Article deleted", Encoding.UTF8, "application/json");             
                return ResponseMessage(response);

            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex));
            }
        }
    }
}
