﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.DB.Entities;
using Blog.DB.Interfaces;
using BlogWebsite.Respository;

namespace BlogWebsite.Controllers
{
    public class BlogController : Controller
    {
        IDatabaseRepository _dbRepo;
        public BlogController(IDatabaseRepository dbRepository)
        {
            _dbRepo = dbRepository;
        }

        // GET: Blog
        public ActionResult Index()
        {
            var articles = _dbRepo.GetAllArticles();
            return View("Index", articles);
        }
        [Authorize(Roles ="Admin, Editor")]
        public ActionResult Create()
        {

            return View();

        }
        [HttpPost]
        [Authorize(Roles = "Admin, Editor")]
        public ActionResult Create(Article article)
        {

            _dbRepo.CreateArticle(article);
            return RedirectToAction("Index");

        }
        [Authorize(Roles = "Admin, Editor")]

        public ActionResult Update(int id)
        {
            var article = _dbRepo.GetArticleById(id);
            return View(article);

        }
        [HttpPost]
        [Authorize(Roles = "Admin, Editor")]
        public ActionResult Update(Article article)
        {
            _dbRepo.UpdateArticle(article);
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Admin, Editor")]
        public ActionResult Delete(int id)
        {
            var article = _dbRepo.GetArticleById(id);
            return View(article);
        }
        [HttpPost]
        [Authorize(Roles = "Admin, Editor")]
        public ActionResult Delete(Article article)
        {

            _dbRepo.DeleteArticle(article);
            return RedirectToAction("Index");

        }
        public ActionResult Details(int id)
        {
            var article = _dbRepo.GetArticleById(id);
            return View(article);
        }

        [HttpPost]
        public ActionResult UploadImage()
        {
            JsonResult result = new JsonResult();
            var Images = Request.Files;
            for (var i = 0; i < Images.Count; i++)
            {
                var filename = Guid.NewGuid() + Path.GetExtension(Images[i].FileName);
                var path = Server.MapPath("~/Content/Images/") + filename;
                Images[i].SaveAs(path);
                result.Data = (filename);

            }
            
            return result;
        }
    }
}