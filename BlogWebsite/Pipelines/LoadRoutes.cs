﻿using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace BlogWebsite.Pipelines
{
    public class LoadRoutes
    {
        public void Process(PipelineArgs args)
        {
            try
            {
                Log.Info("Load mvc routes processor called", this);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
            }
            catch(Exception ex)
            {
                Log.Error("Failed at processor LoadRoutes", ex, typeof(LoadRoutes));
            }

        }
    }
}