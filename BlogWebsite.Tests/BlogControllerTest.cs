﻿using Blog.DB.Entities;
using Blog.DB.Interfaces;
using BlogWebsite.Controllers;
using Moq;
using NUnit;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BlogWebsite.Tests
{
    [TestFixture]
    public class BlogControllerTest
    {
        List<Article> articleList = null;
        Mock<IDatabaseRepository> dbRepo;
        
        [SetUp]
        public void LoadContext()
        {
            dbRepo = new Mock<IDatabaseRepository>();
            this.articleList = new List<Article>();
            articleList.Add(new Article
            {
                Title = "Test article",
                Description = "Test",
                ID = 1

            });
        }

        [Test]
        public void index_returns_index()
        {

            var obj = new BlogController(dbRepo.Object);
            dbRepo.Setup(ur => ur.GetAllArticles()).Returns(this.articleList);
            var indexResult = obj.Index() as ViewResult;
            Assert.That(indexResult.ViewName, Is.EqualTo("Index"));
        }
    }
}
