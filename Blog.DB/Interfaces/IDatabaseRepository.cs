﻿using Blog.DB.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.DB.Interfaces
{
    public interface IDatabaseRepository
    {
        void CreateArticle(Article article);
        void UpdateArticle(Article article);
        void DeleteArticle(Article article);
        Article GetArticleById(int articleId);
        List<Article> GetAllArticles();

    }
}
