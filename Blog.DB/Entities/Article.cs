﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.DB.Entities
{
    public class Article :BaseEntityModel
    {
     
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public DateTime UpdateDate { get; set; }


    }
}
