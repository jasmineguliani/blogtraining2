﻿using Blog.DB.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.DB
{
    public class BlogDb : IdentityDbContext<BlogUser>
    {
        public BlogDb() : base("name=BlogDatabase")
        {

        }
        public DbSet<Article> Articles { get; set; }

        public static BlogDb Create()
        {
            return new BlogDb();
        }
    }
}
