﻿using Blog.DB;
using Blog.DB.Entities;
using Blog.DB.Interfaces;
using log4net;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog.DB
{
    public class DatabaseRepository : IDatabaseRepository
    {
        BlogDb _db = new BlogDb();
        private static readonly ILog _log = LogManager.GetLogger(typeof(DatabaseRepository)); 
        public void CreateArticle(Article article)
        {
            try
            {
                _db.Articles.Add(article);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error("Error in CreateArticle", ex);
            }

        }
        public void UpdateArticle(Article article)
        {
            try
            {
                _db.Entry(article).State = EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error("Error in UpdateArticle", ex);
            }
        }
        public void DeleteArticle(Article article)
        {
            try
            {

                _db.Articles.Remove(GetArticleById(article.ID));
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error("Error in DeleteArticle", ex);
            }

        }
        public Article GetArticleById(int articleId)
        {
            Article article = new Article();
            try
            {
                article = _db.Articles.Find(articleId);
            }
            catch (Exception ex)
            {
                _log.Error("Error in GetArticleById", ex);
            }
            return article;

        }
        public List<Article> GetAllArticles()
        {
            List<Article> articleList = new List<Article>();
            try
            {
                articleList = _db.Articles.ToList();
            }
            catch (Exception ex)
            {
                _log.Error("Error in UpdateArticle", ex);
            }
            return articleList;

        }
    }
}